
public class Refrigerator {
	public String brand =  "Mitsubishi";
	public double temperature = 5.0;
	public int daysBeforeNextClean = 90;
	
	public String getBrand() {
		return this.brand;
	}
	
	public void modifyTemperature(double degree) {
		this.temperature -= degree;
	}
	
	public void resetCleanDays() {
		this.daysBeforeNextClean = 90;
	}
}
