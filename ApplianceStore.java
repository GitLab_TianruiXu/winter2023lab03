import java.util.Scanner;
public class ApplianceStore {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		Refrigerator[] appliances = new Refrigerator[4];
		for(int i = 0; i < appliances.length; i ++) {
			appliances[i] = new Refrigerator();
			System.out.println("Enter the brand of the refrigerator");
			appliances[i].brand = userInput.nextLine();
			System.out.println("Enter the default temperature of the refrigerator");
			appliances[i].temperature = userInput.nextDouble();
			System.out.println("Enter the default days before next clean of the refrigerator");
			appliances[i].daysBeforeNextClean = userInput.nextInt();
			userInput.nextLine();
		}
		System.out.println("Last Appliance \n" + "Brand name: " + appliances[3].brand + " \n" + 
			"Default temperature: " + appliances[3].temperature + " \n" + 
			"Default days before next clean: " + appliances[3].daysBeforeNextClean);
		System.out.println(appliances[0].getBrand());
		appliances[0].modifyTemperature(-5.0);
		appliances[0].resetCleanDays();
		System.out.println("First Appliance \n" + "Brand name: " + appliances[0].brand + " \n" +
			"Default temperature: " + appliances[0].temperature + " \n" + 
			"Default days before next clean: " + appliances[0].daysBeforeNextClean);
	}

}
